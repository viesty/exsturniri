<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Exs turnīru komandu ģenerators</title>
    <link rel="stylesheet" href="css/foundation.css" />
    <link rel="stylesheet" href="font-awesome/css/font-awesome.css" />
    <script src="js/vendor/modernizr.js"></script>
  </head>
  <body>
    
    <nav class="top-bar" data-topbar role="navigation">
      <ul class="title-area">
        <li class="name">
          <h1><a href="index.php">Exs turnīru komandu ģenerators</a></h1>
        </li>
      </ul>
    </nav>
    <div class="row">
        <div class="small-12 large-12 medium-12 columns">
            <div data-alert class="alert-box warning">Nekāda formu validācija nenotiek, tāpēc raksti uzmanīgi!
                 Nu labi, tagad ir mazliet validācijas, bet vienalga raksti uzmanīgi.
                <a href="#" class="close">&times;</a></div>
        </div>
    </div>
      
    <div class="row" id="details">
        <div class="small-12 large-12 medium-12 columns">
            <label>Cik spēlētāji vienā komandā?
                <input id="playersPT" type="text" placeholder="Spēlētāji vienā komandā" />
            </label>
        </div>
    </div>
    <div class="row" id="playerWarn">
        <div class="small-12 large-12 medium-12 columns">
            <div data-alert class="alert-box alert">Ieraksti spēlētāju skaitu. 
                Heh, tomēr ir validācija kaut kāda! Bet nu ja tu ierakstīsi burtus...
                <a href="#" class="close">&times;</a></div>
        </div>
    </div>
    
    <div class="row">
        <div class="large-12 columns">
            <div class="switch">
                <p>Vai ievietot @ zīmi pirms nika rezultātos?</p>
                <input id="checkAt" type="checkbox">
                <label for="checkAt"></label>
            </div> 
        </div>
    </div>
      
    <div class="row" id="komandas">
        <div class="small-12 large-12 medium-12 columns">
            <label>Spēlētāju niki (atdali ar komatiem, neliekot pēc tiem atstarpes)
                <input id="playerList" type="text" placeholder="niks1,niks2,niks3,..." />
            </label>
        </div>
    </div>
    
    <div class = "row" id="details">
        <div class = "small-12 large-12 medium-6 columns">
            <a href="#" id="generate" class="button expand">Ģenerēt</a>
        </div>
    </div>
    
    <div class="row" id="generatedContent">
    </div>
      
    <script src="js/vendor/jquery.js"></script>
    <script src="js/foundation.min.js"></script>
    <script>
      $(document).foundation();
      
      $(document).ready(function(){
          $('#playerWarn').hide();
          var numTeams, numPlayers, playersPT, playerList = [];
          $('#generate').click(function(){
                playersPT = $.trim($('#playersPT').val());
                $('#generatedContent').empty();
                if(playersPT.length < 1 || playersPT === '0'){
                    $('#playerWarn').show();
                    return 0;
                };
                
                $('#playerWarn').hide();
                playerList = $('#playerList').val().split(',');
                numPlayers = playerList.length;
                numTeams = Math.ceil(numPlayers/playersPT);
                shuffle(playerList);
                for(a = 1; a <= numTeams; a++){
                    $('#generatedContent').append('<p id="team'+a+'">'+a+'. komanda: </p>');
                    for(b = 0; b < playersPT; b++){
                        if($('#checkAt').is(':checked')){
                            if(b!=playersPT-1){
                                if(typeof playerList[b]!='undefined'){
                                    $('#team'+a).append('@'+playerList[b]+', ');
                                }
                            }
                            else{
                                if(typeof playerList[b]!='undefined'){
                                    $('#team'+a).append('@'+playerList[b]);
                                }
                            }
                        }else{
                            if(b!=playersPT-1){
                                if(typeof playerList[b]!='undefined'){
                                    $('#team'+a).append(playerList[b]+', ');
                                }
                            }
                            else{
                                if(typeof playerList[b]!='undefined'){
                                    $('#team'+a).append(playerList[b]);
                                }
                            }
                        }
                    }
                    playerList.splice(0, playersPT);
                }
          });
      });
      
      function shuffle(array) {
        var currentIndex = array.length, temporaryValue, randomIndex ;

        // While there remain elements to shuffle...
        while (0 !== currentIndex) {

          // Pick a remaining element...
          randomIndex = Math.floor(Math.random() * currentIndex);
          currentIndex -= 1;

          // And swap it with the current element.
          temporaryValue = array[currentIndex];
          array[currentIndex] = array[randomIndex];
          array[randomIndex] = temporaryValue;
        }

        return array;
      }
      
    </script>
  </body>
</html>
